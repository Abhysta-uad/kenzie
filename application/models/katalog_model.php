<?php 

class Katalog_model extends CI_Model{
    public $id_barang;
    public $nama_barang;
    public $harga_barang;
    public $stock_barang;
    public $gambar_barang;

    public function __construct()
	{
        $this->load->database();
    }
    
    public function getKatalog()
    {
        $ambil = $this->db->get('katalog');
        return $ambil->result();
    }

    public function addKatalog()
    {
        $data = [
            '$nama_barang' => $this->input->post('nama_barang'),
            '$harga_barang' => $this->input->post('harga_barang'),
            '$stock_barang' => $this->input->post('stock_barang'),
            '$gambar_barang' => $this->input->post('gambar_barang'),
        ];

        $this->db->insert('katalog', $data);
        return $this->db->update('katalog', $data);
    }

    public function editKatalog($id_barang = 0)
    {
        $data = array(
            '$nama_barang' => $this->input->post('nama_barang'),
            '$harga_barang' => $this->input->post('harga_barang'),
            '$stock_barang' => $this->input->post('stock_barang'),
            '$gambar_barang' => $this->input->post('gambar_barang'),
        );
        if ($id_barang == 0) {
			return $this->db->insert('katalog', $data);
		} else {
			$this->db->where('id_barang', $id_barang);
			return $this->db->update('katalog', $data);
		}
    }

    public function deleteKatalog($id_barang)
    {
        $this->db->where('id_barang', $id_barang);
		return $this->db->delete('katalog');
    }
}

?>