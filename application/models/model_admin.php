<?php 


defined('BASEPATH') OR exit('No direct script access allowed');

class Model_admin extends CI_Model {

    public function all_product(){
        return $this->db->get('product')->num_rows();
    }

    public function all_user(){
        return $this->db->get('user')->num_rows();
    }

    public function all_kategori(){
        return $this->db->get('categories')->num_rows();
    }
}