<?php 


defined('BASEPATH') OR exit('No direct script access allowed');

class Model_categories extends CI_Model {

    public function get_categories(){
        $this->db->select('*');
        $this->db->from('categories');
        $this->db->order_by('id_kategori', 'desc');
        return $this->db->get()->result();
        
    }

    public function add($data){
        $this->db->insert('categories', $data);
    }

    public function edit($data){
        $this->db->where('id_kategori', $data['id_kategori']);
        $this->db->update('categories', $data);
    }

    public function delete($data){
        $this->db->where('id_kategori', $data['id_kategori']);
        $this->db->delete('categories', $data);
    }

}

/* End of file ModelName.php */


?>