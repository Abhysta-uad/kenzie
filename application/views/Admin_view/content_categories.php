<ul class="breadcrumbs">
    <li class="nav-home">
        <a href="<?= base_url('admin')?>">
            <i class="flaticon-home"></i>
        </a>
    </li>
    <li class="separator">
        <i class="flaticon-right-arrow"></i>
    </li>
    <li class="nav-item">
        <a href="<?=  base_url('categories')?>"><?= $title?></a>
    </li>
</ul>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Basic</h4>
            </div>
            <div class="card-body">
                <button type="button" class="btn btn-primary mb-2" data-toggle="modal" data-target="#tambah">Tambah
                    Categories<i class="ml-2 fas fa-fw fa-plus"></i>
                </button>

                <?php if($this->session->flashdata('kategori')) {
                    echo '<div class="alert alert-success alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <h5><i class="icon fas fa-check"></i> Success!</h5>';
                    echo $this->session->flashdata('kategori');
                    echo '</div>';
                }?>

                <div class="table-responsive">
                    <table id="basic-datatables" class="display table table-striped table-hover text-center">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Categories</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no=1;
                        foreach ($categories as $c => $value) { ?>
                            <tr>
                                <th><?= $no++ ?></th>
                                <th><?= $value->nama_kategori ?></th>
                                <th>
                                    <button class="btn btn-warning btn-sm m-1" data-toggle="modal"
                                        data-target="#edit<?= $value->id_kategori ?>"><i
                                            class="fas fa-edit"></i></button>
                                    <button class="btn btn-danger btn-sm m-1" data-toggle="modal"
                                        data-target="#hapus<?= $value->id_kategori ?>"><i
                                            class="fas fa-trash-alt"></i></button>
                                </th>
                            </tr>
                            <?php } ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- modal tambah -->
<div class="modal fade" id="tambah">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah Categories</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php echo form_open('categories/add') ?>

                <div class="form-group">
                    <label>Nama Kategori</label>
                    <input type="text" name="nama_kategori" class="form-control" placeholder="Enter Categories"
                        required>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
            <?php echo form_close() ?>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<!-- modal edit -->
<?php foreach ($categories as $c => $value) { ?>
<div class="modal fade" id="edit<?= $value->id_kategori ?>">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit Categories</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php echo form_open('categories/edit/'. $value->id_kategori) ?>

                <div class="form-group">
                    <label>Categories</label>
                    <input type="text" name="nama_kategori" value="<?= $value->nama_kategori ?>" class="form-control"
                        placeholder="Enter categories" required>
                </div>

            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
            <?php echo form_close() ?>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<?php } ?>



<!-- modal Delete -->
<?php foreach ($categories as $c => $value) { ?>
<div class="modal fade" id="hapus<?= $value->id_kategori ?>">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Delete <?= $value->nama_kategori?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h6>Apakah Anda Yakin Ingin Menghapus Kategori Ini ?</h6>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <a href="<?= base_url('categories/delete/'. $value->id_kategori) ?>" class="btn btn-primary">Delete</a>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<?php } ?>