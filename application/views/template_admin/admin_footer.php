</div>
</div>
</div>

</div>

<!--   Core JS Files   -->
<script src="<?=base_url('assets/admin')?>/assets/js/core/jquery.3.2.1.min.js"></script>
<script src="<?=base_url('assets/admin')?>/assets/js/core/popper.min.js"></script>
<script src="<?=base_url('assets/admin')?>/assets/js/core/bootstrap.min.js"></script>

<!-- jQuery UI -->
<script src="<?=base_url('assets/admin')?>/assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
<script src="<?=base_url('assets/admin')?>/assets/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js">
</script>

<!-- jQuery Scrollbar -->
<script src="<?=base_url('assets/admin')?>/assets/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js"></script>

<!-- Moment JS -->
<script src="<?=base_url('assets/admin')?>/assets/js/plugin/moment/moment.min.js"></script>

<!-- Chart JS -->
<script src="<?=base_url('assets/admin')?>/assets/js/plugin/chart.js/chart.min.js"></script>

<!-- jQuery Sparkline -->
<script src="<?=base_url('assets/admin')?>/assets/js/plugin/jquery.sparkline/jquery.sparkline.min.js"></script>

<!-- Chart Circle -->
<script src="<?=base_url('assets/admin')?>/assets/js/plugin/chart-circle/circles.min.js"></script>

<!-- Datatables -->
<script src="<?=base_url('assets/admin')?>/assets/js/plugin/datatables/datatables.min.js"></script>

<!-- Bootstrap Notify -->
<script src="<?=base_url('assets/admin')?>/assets/js/plugin/bootstrap-notify/bootstrap-notify.min.js"></script>

<!-- Bootstrap Toggle -->
<script src="<?=base_url('assets/admin')?>/assets/js/plugin/bootstrap-toggle/bootstrap-toggle.min.js"></script>

<!-- jQuery Vector Maps -->
<script src="<?=base_url('assets/admin')?>/assets/js/plugin/jqvmap/jquery.vmap.min.js"></script>
<script src="<?=base_url('assets/admin')?>/assets/js/plugin/jqvmap/maps/jquery.vmap.world.js"></script>

<!-- Google Maps Plugin -->
<script src="<?=base_url('assets/admin')?>/assets/js/plugin/gmaps/gmaps.js"></script>

<!-- Sweet Alert -->
<script src="<?=base_url('assets/admin')?>/assets/js/plugin/sweetalert/sweetalert.min.js"></script>

<!-- Azzara JS -->
<script src="<?=base_url('assets/admin')?>/assets/js/ready.min.js"></script>

<!-- Azzara DEMO methods, don't include it in your project! -->
<script src="<?=base_url('assets/admin')?>/assets/js/setting-demo.js"></script>
<script src="<?=base_url('assets/admin')?>/assets/js/demo.js"></script>

<script>
function bacaGambar(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#gambar_load').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

$("#preview_gambar").change(function() {
    bacaGambar(this);
});

window.setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function() {
        $(this).remove();
    });
}, 3000)
</script>

</body>

</html>