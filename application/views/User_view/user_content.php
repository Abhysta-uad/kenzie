<div class="card">
    <div class="card-body">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100" src="<?= base_url();?>assets/a11.jpg" alt="First slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="<?= base_url();?>assets/a22.jpg" alt="Second slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="<?= base_url();?>assets/b22.png" alt="Third slide">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-custom-icon" aria-hidden="true">
                    <i class="fas fa-chevron-left"></i>
                </span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-custom-icon" aria-hidden="true">
                    <i class="fas fa-chevron-right"></i>
                </span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>

<!-- Default box -->

<div class="row">
    <?php foreach ($product as $p => $value){?>
    <div class="col-12 col-sm-6 col-md-4 d-flex align-items-stretch flex-column">
        <?php
        echo form_open('keranjang/add');
        echo form_hidden('id', $value->id_barang);
        echo form_hidden('qty', 1);
        echo form_hidden('price', $value->harga_barang);
        echo form_hidden('name', $value->nama_barang);
        echo form_hidden('redirect_page', str_replace('index.php/', '', current_url()));
        ?>
        <div class="card card-primary card-outline" > 
            <div class="card-body box-profile">
                <div class="text-center">
                    <img class="img-fluid rounded" src="<?= base_url('assets/product/'. $value->gambar);?>"
                        alt="User profile picture" style="width:500px;height:200px;">
                </div>

                <h3 class="profile-username text-center text-uppercase"><?= $value->nama_barang?></h3>
                <ul class="list-group list-group-unbordered mb-3">
                    <li class="list-group-item">
                        <b>Kategori</b> <a class="float-right"><?= $value->nama_kategori?></a>
                    </li>
                    <li class="list-group-item">
                        <b>Stock Barang</b> <a class="float-right"><?= $value->stock_barang?></a>
                    </li>
                    <li class="list-group-item">
                        <b>Harga</b> <a class="float-right"><?= $value->harga_barang?></a>
                    </li>
                </ul>

                <div class="float-right">

                    <a href="<?= base_url('user/detail_b/'. $value->id_barang)?>" class="btn btn-primary"><b><i class="fas fa-eye mr-1"></i>View</b></a>
                    <button type="submit" class="btn btn-primary toastrDefaultSuccess"><b><i class="fas fa-cart-plus mr-1"></i>Tambah</b></button>
                </div>
            </div>
            <!-- /.card-body -->
        </div>
        <?php echo form_close();?>
    </div>
    <?php } ?>
</div>
