<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= $judul?></title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?= base_url()?>assets/template/plugins/fontawesome-free/css/all.min.css">
    <!-- DataTables -->
    <link rel="stylesheet"
        href="<?= base_url()?>assets/template/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet"
        href="<?= base_url()?>assets/template/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet"
        href="<?= base_url()?>assets/template/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?= base_url()?>assets/template/dist/css/adminlte.min.css">

    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
</head>

<body class="hold-transition layout-top-nav">
    <div class="wrapper">
        <!-- Navbar -->
        <nav class="navbar navbar-expand navbar-primary navbar-dark">
            <!-- Left navbar links -->
            <div class="container">
                <ul class="navbar-nav">
                <li class="nav-item d-none d-sm-inline-block">
                        <h5><a href="<?= base_url('user')?>" class="nav-link"><i class="fab fa-kickstarter-k" style="color: yellow"></i>Kenzie</a></h5>
                    </li>
                    <li class="nav-item d-none d-sm-inline-block">
                        <a href="<?= base_url('user')?>" class="nav-link">Home</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false" class="nav-link dropdown-toggle">Categories</a>
                        <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
                            <li><a href="#" class="dropdown-item">Some action </a></li>
                            <li><a href="#" class="dropdown-item">Some other action</a></li>

                            <li class="dropdown-divider"></li>

                            <!-- Level two dropdown-->
                            <li class="dropdown-submenu dropdown-hover">
                                <a id="dropdownSubMenu2" href="#" role="button" data-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="false"
                                    class="dropdown-item dropdown-toggle">Hover for action</a>
                                <ul aria-labelledby="dropdownSubMenu2" class="dropdown-menu border-0 shadow">
                                    <li>
                                        <a tabindex="-1" href="#" class="dropdown-item">level 2</a>
                                    </li>

                                    <!-- Level three dropdown-->
                                    <li class="dropdown-submenu">
                                        <a id="dropdownSubMenu3" href="#" role="button" data-toggle="dropdown"
                                            aria-haspopup="true" aria-expanded="false"
                                            class="dropdown-item dropdown-toggle">level 2</a>
                                        <ul aria-labelledby="dropdownSubMenu3" class="dropdown-menu border-0 shadow">
                                            <li><a href="#" class="dropdown-item">3rd level</a></li>
                                            <li><a href="#" class="dropdown-item">3rd level</a></li>
                                        </ul>
                                    </li>
                                    <!-- End Level three -->

                                    <li><a href="#" class="dropdown-item">level 2</a></li>
                                    <li><a href="#" class="dropdown-item">level 2</a></li>
                                </ul>
                            </li>
                            <!-- End Level two -->
                        </ul>
                    </li>
                </ul>

                <!-- Right navbar links -->
                <ul class="navbar-nav ml-auto">
                    <!-- Navbar Search -->
                    <li class="nav-item">
                        <a class="nav-link" data-widget="navbar-search" data-target="#navbar-search3" href="#"
                            role="button">
                            <i class="fas fa-search"></i>
                        </a>
                        <div class="navbar-search-block" id="navbar-search3">
                            <form class="form-inline">
                                <div class="input-group input-group-sm">
                                    <input class="form-control form-control-navbar" type="search" placeholder="Search"
                                        aria-label="Search">
                                    <div class="input-group-append">
                                        <button class="btn btn-navbar" type="submit">
                                            <i class="fas fa-search"></i>
                                        </button>
                                        <button class="btn btn-navbar" type="button" data-widget="navbar-search">
                                            <i class="fas fa-times"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </li>

                    <!-- Messages Dropdown Menu -->
                    <?php $keranjang= $this->cart->contents(); 
                    $jml_barang = 0;
                    foreach ($keranjang as $k => $value) {
                        $jml_barang = $jml_barang + $value['qty'];
                    }                    
                    ?>
                    <li class="nav-item dropdown">
                        <a class="nav-link" data-toggle="dropdown" href="#">
                            <i class="fas fa-cart-plus"></i>
                            <span class="badge badge-danger navbar-badge"><?= $jml_barang?></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right mt-2">
                            <?php if (empty($keranjang)) { ?>
                            <a href="#" class="dropdown-item">
                                <p>Keranjang Belanja Kosong</p>
                            </a>
                            <?php } else {
                                foreach ($keranjang as $k => $value) {
                                $product = $this->model_user->detail($value['id']);                                
                            ?>
                            <!-- Barang Start -->
                            <a href="#" class="dropdown-item">
                                <div class="media">
                                    <img src="<?= base_url('assets/product/'. $product->gambar);?>" alt="User Avatar"
                                        class="img-size-50 mr-3">
                                    <div class="media-body">
                                        <h3 class="dropdown-item-title">
                                            <?= $value['name']?>
                                        </h3>
                                        <p class="text-sm"><?= $value['qty']?> x Rp.
                                            <?= number_format($value['price'], 0)?></p>
                                        <p class="text-sm text-muted"><i class="fas fa-tags"></i> Rp.
                                            <?= $this->cart->format_number($value['subtotal']);?></p>
                                    </div>
                                </div>
                            </a>
                            <div class="dropdown-divider"></div>
                            <?php } ?>

                            <a href="#" class="dropdown-item">
                                <div class="media">
                                    <div class="media-body">
                                        <tr>
                                            <td class="right"><strong>Total</strong></td>
                                            <td class="right">
                                                Rp. <?php echo $this->cart->format_number($this->cart->total()); ?></td>
                                        </tr>
                                    </div>
                                </div>
                            </a>

                            <div class="dropdown-divider"></div>
                            <a href="<?= base_url('keranjang');?>" class="dropdown-item dropdown-footer">Keranjang</a>
                            <a href="<?= base_url('keranjang/pembayaran')?>" class="dropdown-item dropdown-footer">Check Out</a>

                            <?php } ?>
                            <!-- Barang End -->


                        </div>
                    </li>

                    <!-- Notifications Dropdown Menu -->
                    <li class="nav-item dropdown">
                        <a class="nav-link" data-toggle="dropdown" href="#">
                            <i class="fas fa-user"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right mt-2">
                            <div class="text-center mt-3">
                                <img src="<?= base_url();?>assets/template/dist/img/useric.png"
                                    class="img-circle elevation-2 img-fluid img-size-64" alt="User Image">
                                <h3><?= $user['name']?></h3>
                                <p><?= $user['email']?></p>
                            </div>
                            <div class="mt-1">
                                <a href="#" class="dropdown-item">
                                    <i class="fas fa-fw fa-user-edit mr-2"></i>Edit Profile
                                </a>
                                <div class="dropdown-divider"></div>
                                <a href="#" class="dropdown-item">
                                    <i class="fas fa-fw fa-info mr-2"></i>About
                                </a>
                                <div class="dropdown-divider"></div>
                                <a href="<?= base_url('auth/logout')?>" class="dropdown-item">
                                    <i class="fas fa-fw fa-sign-out-alt mr-2"></i>Logout
                                </a>
                                <div class="dropdown-divider"></div>
                                <a href="#" class="dropdown-item dropdown-footer">Close</a>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
        <!-- /.navbar -->