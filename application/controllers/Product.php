<?php 



defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        //Load Dependencies
        $this->load->model('model_product');
        $this->load->model('model_categories');

    }

    // List all your items
    public function index( $offset = 0 )
    {
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data ['judul']= 'Halaman Administrator';
        $data ['title']= 'Product';
        $data['product'] = $this->model_product->get_product();

        $this->load->view('template_admin/admin_header', $data);
        $this->load->view('admin_view/index');
        $this->load->view('admin_view/content_product');
        $this->load->view('template_admin/admin_footer');
    }

    // Add a new item
    public function addproduct()
    {
        $this->form_validation->set_rules('nama_barang', 'Nama Barang', 'required', array(
            'required' => 'Masukkan Nama Barang !'
        ));
        $this->form_validation->set_rules('id_kategori', 'Kategori', 'required', array(
            'required' => 'Pilih Kategori !'
        ));
        $this->form_validation->set_rules('stock_barang', 'Stock Barang', 'required', array(
            'required' => 'Masukkan Stock Barang !'
        ));
        $this->form_validation->set_rules('harga_barang', 'Harga Barang', 'required', array(
            'required' => 'Masukkan Harga Barang !'
        ));
        $this->form_validation->set_rules('detail', 'Detail', 'required', array(
            'required' => 'Masukkan Detail Barang !'
        ));

        
        if ($this->form_validation->run() == TRUE) {
            $config['upload_path'] = './assets/product/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg|ico';
            $config['max_size']     = '2000';

            $this->upload->initialize($config);
            $g = "gambar";

            if (!$this->upload->do_upload($g)) {
                $data = array (
                    'user' => $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array(),
                    'judul'=> 'Halaman Administrator',
                    'title'=> 'Product',
                    'title1'=> 'Product',
                    'title2'=> 'Tambah Product',
                    'categories'=> $this->model_categories->get_categories(),
                    'error_up'=> $this->upload->display_errors(),

                );

                $this->load->view('template_admin/admin_header', $data);
                $this->load->view('admin_view/index');
                $this->load->view('admin_view/content_product_add');
                $this->load->view('template_admin/admin_footer');
            } else {
                $up_gambar = array('uploads' => $this->upload->data());
                $config['image_library'] = 'gd2';
                $config['source_image'] = './assets/product/'. $up_gambar['uploads']['file_name'];
                $this->load->library('image_lib', $config);
                $data = array(
                    'nama_barang' => $this->input->post('nama_barang'),
                    'id_kategori' => $this->input->post('id_kategori'),
                    'stock_barang' => $this->input->post('stock_barang'),
                    'harga_barang' => $this->input->post('harga_barang'),
                    'detail' => $this->input->post('detail'),
                    'gambar' => $up_gambar['uploads']['file_name'],
                );
                $this->model_product->add($data);
                $this->session->set_flashdata('barang', 'Data Berhasil Di Tambahkan');
                redirect('product');
            }
        } else {

            $data = array (
                        'user' => $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array(),
                        'judul'=> 'Halaman Administrator',
                        'title'=> 'Product',
                        'title1'=> 'Product',
                        'title2'=> 'Tambah Product',
                        'categories'=> $this->model_categories->get_categories(),
                    );
    
                    $this->load->view('template_admin/admin_header', $data, false);
                    $this->load->view('admin_view/index', false);
                    $this->load->view('admin_view/content_product_add', false);
                    $this->load->view('template_admin/admin_footer', false);
        }

    }

    //Update one item
    public function edit( $id_barang = NULL )
    {
        $this->form_validation->set_rules('nama_barang', 'Nama Barang', 'required', array(
            'required' => 'Masukkan Nama Barang !'
        ));
        $this->form_validation->set_rules('id_kategori', 'Kategori', 'required', array(
            'required' => 'Pilih Kategori !'
        ));
        $this->form_validation->set_rules('stock_barang', 'Stock Barang', 'required', array(
            'required' => 'Masukkan Stock Barang !'
        ));
        $this->form_validation->set_rules('harga_barang', 'Harga Barang', 'required', array(
            'required' => 'Masukkan Harga Barang !'
        ));
        $this->form_validation->set_rules('detail', 'Detail', 'required', array(
            'required' => 'Masukkan Detail Barang !'
        ));

        
        if ($this->form_validation->run() == TRUE) {
            $config['upload_path'] = './assets/product/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg|ico';
            $config['max_size']     = '2000';

            $this->upload->initialize($config);
            $g = "gambar";

            if (!$this->upload->do_upload($g)) {
                $data = array (
                    'user' => $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array(),
                    'judul'=> 'Halaman Administrator',
                    'title'=> 'Product',
                    'title1'=> 'Product',
                    'title2'=> 'Edit Product',
                    'product' => $this->model_product->get_data($id_barang),
                    'categories'=> $this->model_categories->get_categories(),
                    'error_up'=> $this->upload->display_errors(),

                );

                $this->load->view('template_admin/admin_header', $data);
                $this->load->view('admin_view/index');
                $this->load->view('admin_view/content_product_edit');
                $this->load->view('template_admin/admin_footer');
            } else {
                //hapus gambar
                $product = $this->model_product->get_data($id_barang);
                if ($product->gambar != "") {
                    unlink('./assets/product/'. $product->gambar);
                }
                //end
                $up_gambar = array('uploads' => $this->upload->data());
                $config['image_library'] = 'gd2';
                $config['source_image'] = './assets/product/'. $up_gambar['uploads']['file_name'];
                $this->load->library('image_lib', $config);
                $data = array(
                    'id_barang' => $id_barang,
                    'nama_barang' => $this->input->post('nama_barang'),
                    'id_kategori' => $this->input->post('id_kategori'),
                    'stock_barang' => $this->input->post('stock_barang'),
                    'harga_barang' => $this->input->post('harga_barang'),
                    'detail' => $this->input->post('detail'),
                    'gambar' => $up_gambar['uploads']['file_name'],
                );
                $this->model_product->edit($data);
                $this->session->set_flashdata('barang', 'Data Berhasil Di Edit');
                redirect('product');
            }
            //Tampa Mengganti Gambar
            $data = array(
                    'id_barang' => $id_barang,
                    'nama_barang' => $this->input->post('nama_barang'),
                    'id_kategori' => $this->input->post('id_kategori'),
                    'stock_barang' => $this->input->post('stock_barang'),
                    'harga_barang' => $this->input->post('harga_barang'),
                    'detail' => $this->input->post('detail'),
                );
                $this->model_product->edit($data);
                $this->session->set_flashdata('barang', 'Data Berhasil Di Edit');
                redirect('product');
        } else {

            $data = array (
                        'user' => $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array(),
                        'judul'=> 'Halaman Administrator',
                        'title'=> 'Product',
                        'title1'=> 'Product',
                        'title2'=> 'Edit Product',
                        'product' => $this->model_product->get_data($id_barang),
                        'categories'=> $this->model_categories->get_categories(),
                    );
    
                    $this->load->view('template_admin/admin_header', $data, false);
                    $this->load->view('admin_view/index', false);
                    $this->load->view('admin_view/content_product_edit', false);
                    $this->load->view('template_admin/admin_footer', false);
        }

    }

    //Delete one item
    public function delete( $id_barang = NULL )
    {
        //hapus gambar
        $product = $this->model_product->get_data($id_barang);
        if ($product->gambar != "") {
            unlink('./assets/product/'. $product->gambar);
        }
        //end
        $data = array('id_barang' => $id_barang );
        $this->model_product->delete($data);
        $this->session->set_flashdata('barang', 'Data Berhasil Di Hapus');
         redirect('product');
    }
}

/* End of file Product.php */



?>