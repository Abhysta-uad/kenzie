<?php 

class Auth extends CI_Controller{

    
    public function index(){
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'Password','trim|required');
        if($this->form_validation->run() == false){
            $data['judul'] = 'Halaman Login';
            $this->load->view("templates/auth_header", $data);
            $this->load->view("auth/login");
            $this->load->view("templates/auth_footer");
        }
        else{
            $this->plogin();
        }
    }

    private function plogin(){
        $email = $this->input->post('email');
        $password = $this->input->post('password');

        $user = $this->db->get_where('user', ['email' => $email])->row_array();
        
        //jika user ada
        if($user){
            //jika user active
            if($user['is_active'] == 1) {
                if(password_verify($password, $user['password'])){
                    $data = [
                        'email' => $user['email'],
                        'role_id' => $user['role_id']
                    ];
                    $this->session->set_userdata($data);

                    if($user['role_id'] == 1) {
                        redirect('admin');
                    }
                    else {
                        redirect('user');
                    }
                }
                else{
                    $this->session->set_flashdata('auth','<div class="alert alert-danger" role="alert">Password anda salah</div>');
            redirect('auth');
                }
            }
            else{
                $this->session->set_flashdata('auth','<div class="alert alert-danger" role="alert">Email belum teraktivasi</div>');
            redirect('auth');
            }
        }
        else {
            $this->session->set_flashdata('auth','<div class="alert alert-danger" role="alert">Email atau Password anda salah</div>');
            redirect('auth');
        }
    }

    public function register(){

        $this->form_validation->set_rules('name', 'Name', 'required|trim');
        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[user.email]',[
            'is_unique' => 'Email sudah digunakan',
        ]);
        $this->form_validation->set_rules('password1', 'Password', 'required|trim|min_length[8]|matches[password2]',[
            'matches' => 'Password tidak sama!',
            'min_length' => 'Password terlalu pendek!'
        ]);
        $this->form_validation->set_rules('password2', 'Password', 'required|trim|matches[password1]');

        if($this->form_validation->run() == false){
            $data['judul'] = 'Halaman Register';
            $this->load->view("templates/auth_header", $data);
            $this->load->view("auth/register");
            $this->load->view("templates/auth_footer");

        }
        else{
            $data = [
                'name' => htmlspecialchars($this->input->post('name', true)),
                'email' => htmlspecialchars($this->input->post('email', true)),
                'password' => password_hash($this->input->post('password1'), PASSWORD_DEFAULT),
                'role_id' => 2,
                'is_active' => 1,
            ];

            $this->db->insert('user', $data);
            $this->session->set_flashdata('auth','<div class="alert alert-success" role="alert">Selamat!, Akun berhasil dibuat</div>');
            redirect('auth');
        }
    }

    public function logout(){
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('role_id');
        $this->session->set_flashdata('auth','<div class="alert alert-success" role="alert">Kamu berhasil logout</div>');
        redirect('auth');
    }
}

?>