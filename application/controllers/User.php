<?php 

class User extends CI_Controller{

    
    public function __construct()
    {
        parent::__construct();
        //Do your magic here
        $this->load->model("model_user");
    }
    
    
    public function index()
    {
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data ['judul']= 'Halaman Dashboard';
        $data['product'] = $this->model_user->get_product();

        $this->load->view('template_user/user_header', $data);
        $this->load->view('user_view/index', $data);
        $this->load->view('user_view/user_content', $data);
        $this->load->view('template_user/user_footer');
    }

    public function detail_b($id_barang){
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data ['judul']= 'Detail Barang';
        $data['product'] = $this->model_user->detail($id_barang);

        $this->load->view('template_user/user_header', $data);
        $this->load->view('user_view/index', $data);
        $this->load->view('user_view/user_content_detail', $data);
        $this->load->view('template_user/user_footer');
    }
}

?>