<?php 

class Admin extends CI_Controller{

    public function __construct()
    {
        parent::__construct();
        //Load Dependencies
        $this->load->model('model_admin');

    }
    
    public function index()
    {
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data ['judul']= 'Halaman Administrator';
        $data['title'] = 'Dashboard Admin';
        $data['product'] = $this->model_admin->all_product();
        $data['user1'] = $this->model_admin->all_user();
        $data['categories'] = $this->model_admin->all_kategori();

        $this->load->view('template_admin/admin_header', $data);
        $this->load->view('admin_view/index');
        $this->load->view('admin_view/content_admin');
        $this->load->view('template_admin/admin_footer');
    }    
}

?>