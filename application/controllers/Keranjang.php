<?php 

class Keranjang extends CI_Controller{

    
    public function __construct()
    {
        parent::__construct();
    }
    
    
    public function index()
    {
        if (empty($this->cart->contents())) {
            redirect('user');
        }
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data ['judul']= 'Keranjang';

        $this->load->view('template_user/user_header', $data);
        $this->load->view('user_view/index', $data);
        $this->load->view('user_view/user_content_keranjang', $data);
        $this->load->view('template_user/user_footer');
    }

    public function add(){
        $redirect_page = $this->input->post('redirect_page');
        $data = array(
            'id'      => $this->input->post('id'),
            'qty'     => $this->input->post('qty'),
            'price'   => $this->input->post('price'),
            'name'    => $this->input->post('name'),
    );
    
    $this->cart->insert($data);
    redirect($redirect_page, 'refresh');
    }

    public function delete($rowid){
        $this->cart->remove($rowid);
        redirect('keranjang');
    }

    public function update(){
        $i = 1;
        foreach ($this->cart->contents() as $items) {
            $data = array(
                'rowid' => $items['rowid'],
                'qty'   => $this->input->post($i. '[qty]'),
            );
            $this->cart->update($data);
            $i++;
        }
        redirect('keranjang');
    }

    public function deleteall(){
        $this->cart->destroy();
        redirect('user');
    }

    public function pembayaran()
    {
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data ['judul']= 'Pembayaran';

        $this->load->view('template_user/user_header', $data);
        $this->load->view('user_view/index', $data);
        $this->load->view('user_view/user_content_pembayaran', $data);
        $this->load->view('template_user/user_footer');
    }

    public function pemesanan()
    {
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data ['judul']= 'Pembayaran';
        $this->cart->destroy();

        $this->load->view('template_user/user_header', $data);
        $this->load->view('user_view/index', $data);
        $this->load->view('user_view/user_content_pemesanan', $data);
        $this->load->view('template_user/user_footer');
    }
}