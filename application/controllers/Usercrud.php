<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Usercrud extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        //Load Dependencies
        $this->load->model('model_usercrud');
        

    }

    // List all your items
    public function index( $offset = 0 )
    {
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data ['judul']= 'Halaman Administrator';
        $data ['title']= 'User';
        $data['ucrud'] = $this->model_usercrud->get_user();

        $this->load->view('template_admin/admin_header', $data);
        $this->load->view('admin_view/index');
        $this->load->view('admin_view/content_user');
        $this->load->view('template_admin/admin_footer');
    }

    // Add a new item
    public function add()
    {
        $data = array(
            'name' => $this->input->post('name'),
            'email' => $this->input->post('email'),
            'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
            'role_id' => $this->input->post('role_id'),
            'is_active' => $this->input->post('is_active'),
         );
         $this->model_usercrud->add($data);
         $this->session->set_flashdata('usr', 'Data Berhasil Di Tambahkan');
         redirect('usercrud');
         
    }

    //Update one item
    public function edit( $id_user = NULL )
    {
        $data = array(
            'id_user' => $id_user,
            'name' => $this->input->post('name'),
            'email' => $this->input->post('email'),
            'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
            'role_id' => $this->input->post('role_id'),
            'is_active' => $this->input->post('is_active'),
         );
         $this->model_usercrud->edit($data);
         $this->session->set_flashdata('usr', 'Data Berhasil Di Edit');
         redirect('usercrud');
    }

    //Delete one item
    public function delete( $id_user = NULL )
    {
        $data = array('id_user' => $id_user );
        $this->model_usercrud->delete($data);
        $this->session->set_flashdata('usr', 'Data Berhasil Di Hapus');
         redirect('usercrud');
    }
}

/* End of file Controllername.php */



?>